package org.paumard.anagram;

public interface AnagramSolverService {

	boolean isAnagram(String sentence1, String sentence2);
	

}