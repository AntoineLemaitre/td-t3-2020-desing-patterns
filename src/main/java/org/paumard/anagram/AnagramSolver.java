package org.paumard.anagram;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public  class AnagramSolver implements AnagramSolverService,AnagramService {

	
    
	
	@Override
	public boolean isAnagram(String sentence1, String sentence2) {
    	System.out.println(sentence1+" & "+sentence2);
        sentence1=sentence1.toLowerCase().replaceAll(" ", "");
        sentence2=sentence2.toLowerCase().replaceAll(" ", "");
    	if (sentence1.isEmpty() && sentence2.isEmpty()) {
			return true;
		}
    	if (sentence1.equals(sentence2)) {
    		return true;
		}
    	if (sentence1.length()!=sentence2.length())
    	{
    		return false;
    	}
    	if (!sentence1.equals(sentence2)) {
    		
        	
        	for (int i = 0; i < sentence2.length(); ) {
        		System.out.println(sentence1+" & "+sentence2);
        		if (!sentence1.contains(sentence2.subSequence(i, i+1))) {
        			return false;
        		}
        		sentence1=sentence1.replaceFirst((String) sentence2.subSequence(i, i+1), "");
        		sentence2=sentence2.replaceFirst((String) sentence2.subSequence(i, i+1), "");
			}
        	return true;
		}

    	return false;
    }

	Function<String, String> sortCharFunction=sentence->{
		char[] str=sentence.toLowerCase().toCharArray();
		Arrays.sort(str);
		String newStr=new String(str);
		return newStr.trim();
		};
	
	@Override
	public Set<String> findAnagramSetFrom(Set<String> words) {
		// TODO Auto-generated method stub
		
		
		String fileName="files/wordlist_cleaned.txt";
		
		
		Path path = Paths.get(fileName);
		try (Stream<String> lines = Files.lines(path)) {
			
		List<String> mots=lines.collect(Collectors.toList());
		Map<String, List<String>> map=mots.stream().collect(Collectors.groupingBy(sortCharFunction));
		System.out.println(map);
		System.out.println("^^ map des Annagrammes^^");
		words=map.keySet();
		
		} catch (IOException e) {
			
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return words;
	}

	@Override
	public Set<String> findLongestAnagramFrom(Set<String> words) {
		// TODO Auto-generated method stub
String fileName="files/wordlist_cleaned.txt";
		
		
		Path path = Paths.get(fileName);
		try (Stream<String> lines = Files.lines(path)) {
			
		List<String> mots=lines.collect(Collectors.toList());
		Map<String, List<String>> map=mots.stream().collect(Collectors.groupingBy(sortCharFunction));
		
		map.entrySet().forEach(liste->{
			if (liste.getValue().size()>=7) {
				System.out.println(liste);
			}
		});
		System.out.println("^^ Liste les plus longues des Annagrammes^^");
		
		} catch (IOException e) {
			
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return words;
		
	}
	
	
	
}
