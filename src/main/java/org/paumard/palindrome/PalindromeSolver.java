package org.paumard.palindrome;

public class PalindromeSolver {

    public boolean isPalindrome(String sentence) {
    	System.out.println(" Phrase à tester : "+sentence);
    	sentence=sentence.toLowerCase().replaceAll(" ", "");
      System.out.println(" Phrase à tester transformée : "+sentence);
    	if (sentence.length()<=1) {
			return true;
		}
    	if (sentence.length()==2) {
			if (sentence.charAt(0)!=sentence.charAt(1)) {
				return false;
			}
			if (sentence.charAt(0)==sentence.charAt(1)) {
				return true;
			}
		}
    	if (sentence.length()%2==0) {

    		for (int i = 0; i < sentence.length(); i++) {
    			if (sentence.charAt(i)!=sentence.charAt(sentence.length()-1-i)) {
    				return false;
    				
    			}
    		}
    		return true;
		}
    	if (sentence.length()%2==1)
    	{

    		for (int i = 0; i < sentence.length(); i++) {
    			if (sentence.charAt(i)!=sentence.charAt(sentence.length()-1-i)) {
    				return false;
    				
    			}
    		}
			return true;
		}
    	
    	System.out.println(" Renvoi faux pas de if valide");
    	return false;
    }
}
