package org.paumard.operation;

public class Division implements LienOpe{

	@Override
	public String toString() {
		return "Division [operation=" + operation + ", getOperation()=" + getOperation() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String operation="3 / 5 * 100";
}
