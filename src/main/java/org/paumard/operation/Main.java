package org.paumard.operation;

import classetest.Test;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		OperationTypeFactory factory = new OperationTypeFactoryImpl() ;
		LienOpe addition = factory.createAddition();
		OperationSolver Resulat= new OperationSolver();
		
		
		/**
		 * Problem : OperationType --> Addition --> OperationTypeFactory --> OperationTypeFactoryImpl
		 * 							  Soustraction
		 * 							  Multiplication
		 * 							  Division
		 * 
		 * Solution : 	Addition --> OperationType --> OperationTypeFactory --> OperationTypeFactoryImpl
		 * 				Soustraction
		 * 				Multiplication
		 * 				Division
		 * 
		 * Resolu! 
		 */
		System.out.println(addition.toString());System.out.println();
		
		System.out.println(factory.toString());System.out.println();
		System.out.println(Resulat.toString());System.out.println();
		
		Resulat.solve("100 - 1 / 3 + 2 - 3 - 4 + 5 * 10 + 100 / 50");System.out.println();
		Resulat.solve(((Addition) addition).getOperation());
		/**
		 * dépendance : on va créer une classe dans un autre projet et essayé ici
		 * 
		 * Test est la classe qui se trouve dans un autre projet.
		 * 
		 */
		Test dependance = new Test();
		dependance.Testos(); // doit afficher ça marche!

	}

}
