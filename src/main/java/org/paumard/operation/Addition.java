package org.paumard.operation;

public class Addition implements LienOpe{
	
	public String operation="3 + 5 + 100 / 3 + 14";

	public String getOperation() {
		
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
		
	}

	@Override
	public String toString() {
		return "Addition [operation=" + operation + ", getOperation()=" + getOperation() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}


}
