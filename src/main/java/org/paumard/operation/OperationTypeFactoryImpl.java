package org.paumard.operation;

public class OperationTypeFactoryImpl extends OperationTypeFactory {

	@Override
	public String toString() {
		return "OperationTypeFactoryImpl [createAddition()=" + createAddition() + ", createSoutraction()="
				+ createSoutraction() + ", createMultiplication()=" + createMultiplication() + ", createDivision()="
				+ createDivision() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

}
