package org.paumard.operation;

import java.util.ArrayList;
import java.util.List;

public class OperationSolver {

    @Override
	public String toString() {
		return "OperationSolver [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	public double solve(String operation) {
    	var opSplit=operation.split(" ");
    	List<Integer> nbList= new ArrayList<Integer>();
    	List<String> signeOp = new ArrayList<String>();
    	
    	extracted(opSplit, nbList, signeOp);
    	
    	
    	if (nbList.size()<=1) {
			return nbList.get(0);
		}
    	
    	var Resultat=0.0;
    	
    	Resultat = extracted(nbList, signeOp, Resultat);
    	System.out.println(operation+" = "+Resultat);
        return Resultat;
    }

	private double extracted(List<Integer> nbList, List<String> signeOp, double Resultat) {
		var A=0.0;
		var B=0.0;
		for (int i = 0; i < signeOp.size(); i=i+1) {
			if (i==0) {
				 A=nbList.get(i);
			}else {
				A=Resultat;
			}
			 B=nbList.get(i+1);
			
			switch (signeOp.get(i).toString()) {
			case "+": {Resultat=A+B;System.out.println(A+"+"+B+"="+Resultat);break;}
			case "-": {Resultat=A-B;System.out.println(A+"-"+B+"="+Resultat);break;}
			case "*": {Resultat=A*B;System.out.println(A+"*"+B+"="+Resultat);break;}
			case "/": {Resultat=A/B;System.out.println(A+"/"+B+"="+Resultat);break;}
			default:
				throw new IllegalArgumentException("Unexpected value: " + signeOp.get(i));
			}
	
		}
		return Resultat;
	}

	private void extracted(String[] opSplit, List<Integer> nbList, List<String> signeOp) {
		for (int i = 0; i < opSplit.length; i++) {
			
			if (i%2==0) {
				nbList.add(Integer.parseInt(opSplit[i]));
			}else {
				signeOp.add(opSplit[i]);
			}
		}
	}
}
