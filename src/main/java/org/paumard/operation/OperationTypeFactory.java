package org.paumard.operation;

public class OperationTypeFactory implements OperationType{

	@Override
	public String toString() {
		return "OperationTypeFactory [createAddition()=" + createAddition() + ", createSoutraction()="
				+ createSoutraction() + ", createMultiplication()=" + createMultiplication() + ", createDivision()="
				+ createDivision() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	public  Addition createAddition() {
		
		Addition addition = new Addition();
		
		return addition;
	} 
	public  Soustraction createSoutraction() {
		
		Soustraction soustraction = new Soustraction();
		return soustraction;
		
	}
	 
	 public  Multiplication createMultiplication() {
		
		 Multiplication multiplication = new Multiplication();
		 return multiplication;
	}
	 
	public  Division createDivision() {
		
		Division division = new Division();
		return division;
	}
}
