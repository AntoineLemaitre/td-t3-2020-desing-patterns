package org.paumard.company;

public class EmployeeFactory implements Employee ,EmployeeUnique{
	
	private String name;
	private String contractType;
	private int age;
	private int salary;
	
	
	

	public static Builder newBuilder() {
		return new Builder();
	}
	
	public static class Builder {
		private String name;
		private int age;
		private int salary;
		private String contractType;
		
		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		public Builder withAge(int age) {
			this.age = age;
			return this;
			
		}

		public Builder withSalary(int salary) {
			this.salary = salary;
			return this;
			
		}
		public Builder withContractType(String Contract) {
			this.contractType = Contract;
			return this;
			
		}

		public Employee build() {
			EmployeeFactory employee = new EmployeeFactory();
			employee.age = age;
			employee.name = name;
			employee.salary = salary;
			employee.contractType=contractType;
			return employee;
		}
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return this.name;
	}

	@Override
	public int age() {
		// TODO Auto-generated method stub
		return this.age;
	}

	@Override
	public String toString() {
		return "\nMon Nom est " + name + ", j'ai " + age + " ans et je gagne " + salary + " € en "+contractType;
	}

	
}
