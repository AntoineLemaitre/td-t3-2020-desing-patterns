package org.paumard.company;

import java.util.ArrayList;
import java.util.List;

import org.paumard.company.DepartmentFactory.Builder;

public class CompanyFactory implements Company {

	private String name;
	private Employee ceo;
	private Employee cfo;
	private Employee cto;
	private List<Department> departements = new ArrayList<>();

	public static Builder newBuilder() {
		return new Builder();
	}

	public static class Builder {

		private String name;
		private Employee ceo;
		private Employee cfo;
		private Employee cto;
		private List<Department> departements = new ArrayList<>();
		
		public Builder withName(String head) {
			this.name = head;
			return this;
		}
		public Builder withCEO(Employee head) {
			this.ceo = head;
			return this;
		}
		public Builder withCFO(Employee head) {
			this.cfo = head;
			return this;
		}
		public Builder withCTO(Employee head) {
			this.cto = head;
			return this;
		}
		public Builder withDepartement(Department head) {
			this.departements.add(head);
			return this;
		}
		
		public Company build() {
			CompanyFactory company= new CompanyFactory();
			company.name= this.name;
			company.ceo=this.ceo;
			company.cto=this.cto;
			company.cfo=this.cfo;
			company.departements.addAll(this.departements);
			
			return company;
		}
	}

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return this.name;
	}

	@Override
	public List<Department> departments() {
		// TODO Auto-generated method stub
		return this.departements;
	}

	@Override
	public Employee ceo() {
		// TODO Auto-generated method stub
		return this.ceo;
	}

	@Override
	public Employee cto() {
		// TODO Auto-generated method stub
		return this.cto;
	}

	@Override
	public Employee cfo() {
		// TODO Auto-generated method stub
		return this.cfo;
	}

	@Override
	public String toString() {
		return "\nLe nom de l'entreprise est: " + name + "\n Le CEO est: " + ceo + "\n Le CFO est: " + cfo + "\n Le CTO est: " + cto + "\n je possède :"
				+ departements;
	}

	@Override
	public void accept(Visitor visiteur) {
		// TODO Auto-generated method stub
		
	}

}
