package org.paumard.company;

import java.util.ArrayList;
import java.util.List;

public class ProjectFactory implements Project {

	private String name;
	private Employee head;
	private List<Employee> employees = new ArrayList<>();
	
	public static Builder newBuilder() {
		return new Builder();
	}
	
	public static class Builder {

		private String name;
		private Employee head;
		private List<Employee> workForce = new ArrayList<>();

		
		
		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		public Builder withHead(Employee head) {
			this.head = head;
			return this;
		}

		public Builder withWorkForceMember(Employee workForceMember) {
			this.workForce.add(workForceMember);
			return this;
		}

		public Project build() {
			
			if (this.workForce.isEmpty()) {
				throw new IllegalArgumentException("La liste employees ne doit pas être vide");
			}
			
			ProjectFactory project = new ProjectFactory();
			project.name = this.name;
			project.head = this.head;
			project.employees.addAll(this.workForce);
			return project;
		}
		
	}
	
	@Override
	public String name() {
		// TODO Auto-generated method stub
		return this.name;
	}

	@Override
	public Employee head() {
		// TODO Auto-generated method stub
		return this.head;
	}

	@Override
	public List<Employee> workForce() {
		// TODO Auto-generated method stub
		return this.employees;
	}

	@Override
	public String toString() {
		return "\n\nLe nom du projet est: " + name + " et est dirigé par: " + head + "\nemployees=" + employees;
	}

}
