package org.paumard.company;

public class AveragingAgesVisitor implements Visitor{

	private long countEmployee = 0L;
	private int sumAges = 0;
	
	public long getCountEmployee() {
		return countEmployee;
	}
	public void setCountEmployee(long countEmployee) {
		this.countEmployee = countEmployee;
	}
	public void visitEmployee(Employee employee) {
		this.countEmployee++;
		this.sumAges += employee.age();

	}
	public double averageAges() {
		return (double)sumAges / (double)countEmployee;
	}
}
