package org.paumard.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**
		 * 
		 
		EmployeeFactory Jack =(EmployeeFactory) EmployeeFactory.newBuilder()
				.withAge(23)
				.withName("Jack")
				.withSalary(1)
				.withContractType("FullTime")
				.build();
		EmployeeFactory Paul =(EmployeeFactory) EmployeeFactory.newBuilder()
				.withAge(23)
				.withName("Paul")
				.withSalary(1)
				.withContractType("HalfTime")
				.build();
		EmployeeFactory Pierre =(EmployeeFactory) EmployeeFactory.newBuilder()
				.withAge(23)
				.withName("pierre")
				.withSalary(2)
				.withContractType("FullTime")
				.build();
		EmployeeFactory Igor =(EmployeeFactory) EmployeeFactory.newBuilder()
				.withAge(23)
				.withName("Popo")
				.withSalary(1)
				.withContractType("FullTime")
				.build();
		//System.out.println(Igor);
		
		EmployeeFactory Antoine =(EmployeeFactory) EmployeeFactory.newBuilder()
				.withAge(23)
				.withName("Paks")
				.withSalary(12)
				.withContractType("FullTime")
				.build();
		//System.out.println(Antoine);
		
		ProjectFactory projet =(ProjectFactory) ProjectFactory.newBuilder()
					.withHead(Antoine)
					.withName("Que du sale Project")
					.withWorkForceMember(Pierre)
					.withWorkForceMember(Paul)
					.withWorkForceMember(Jack)
					.build();
		//System.out.println(projet.toString());
		
		DepartmentFactory Departement = (DepartmentFactory) DepartmentFactory.newBuilder()
				.withProject(projet)
				.withHead(Igor)
				.withManagementMember(Antoine)
				.build();
		System.out.println(Departement.toString());
		*/

		String fileName="files/employee.txt";
		Path path = Paths.get(fileName);
		String fileName2="files/company.txt";
		Path path2 = Paths.get(fileName2);
		try (Stream<String> lines = Files.lines(path);Stream<String> lines2 = Files.lines(path2) ) {
			List<Employee> Employes=new ArrayList<Employee>();
			lines.forEach(ligne->{
				ligne=ligne.replaceAll(" ", ",");
				var ligneSplit=ligne.split(",");
				int age=0;
				String type = null;
				for (int i = 1; i < ligneSplit.length; i++) {
					if (ligneSplit[i].length()==2) {
						age=Integer.parseInt(ligneSplit[i]);
					}
					// j'avais dejà implémenté avant l'xo 3
					if (ligneSplit[i].equals("Part")) {
						type="PartTime";
					}
					if (ligneSplit[i].equals("Full")) {
						type="FullTime";
					}
					if (ligneSplit[i].equals("Contractor")) {
						type="Contractor";
					}
					
				}
				Employes.add((EmployeeFactory) EmployeeFactory.newBuilder()
				.withAge(age)
				.withName(ligneSplit[0])
				.withContractType(type)
				.build());
			});
			CompanyFactory MaBoite=(CompanyFactory) CompanyFactory.newBuilder()
					.withCEO(Employes.get(0))
					.withCTO(Employes.get(1))
					.withCFO(Employes.get(2))
					.withName("Software that works Inc.")
					.withDepartement(DepartmentFactory.newBuilder()
							.withNomDepertement("Finance")
							.withHead(Employes.get(3))
							.withManagementMember(Employes.get(4))
							.withManagementMember(Employes.get(5))
							.withManagementMember(Employes.get(6))
							.withProject( ProjectFactory.newBuilder()
									.withName("Loan Service")
									.withHead(Employes.get(15))
									.withWorkForceMember(Employes.get(16))
									.withWorkForceMember(Employes.get(17))
									
									.build())
							.withProject( ProjectFactory.newBuilder()
									.withName(" Asset Management")
									.withHead(Employes.get(18))
									.withWorkForceMember(Employes.get(19))
									.withWorkForceMember(Employes.get(20))
									.withWorkForceMember(Employes.get(21))
									.build())
							.build()
							)
					.withDepartement(DepartmentFactory.newBuilder()
							.withNomDepertement("Human Ressources")
							.withHead(Employes.get(7))
							.withManagementMember(Employes.get(8))
							.withManagementMember(Employes.get(9))
							.withManagementMember(Employes.get(10))
							.withProject( ProjectFactory.newBuilder()
									.withName("Recruiting")
									.withHead(Employes.get(22))
									.withWorkForceMember(Employes.get(23))
									.withWorkForceMember(Employes.get(24))
									
									.build())
							.withProject( ProjectFactory.newBuilder()
									.withName("Payment")
									.withHead(Employes.get(25))
									.withWorkForceMember(Employes.get(26))
									.withWorkForceMember(Employes.get(27))
									
									.build())
							.build()
							)
					.withDepartement(DepartmentFactory.newBuilder()
							.withNomDepertement("Software")
							.withHead(Employes.get(11))
							.withManagementMember(Employes.get(12))
							.withManagementMember(Employes.get(13))
							.withManagementMember(Employes.get(14))
							.withProject( ProjectFactory.newBuilder()
									.withName("Web Design")
									.withHead(Employes.get(28))
									.withWorkForceMember(Employes.get(29))
									.withWorkForceMember(Employes.get(30))
									
									.build())
							.withProject( ProjectFactory.newBuilder()
									.withName("Database Access")
									.withHead(Employes.get(31))
									.withWorkForceMember(Employes.get(32))
									.withWorkForceMember(Employes.get(33))
									.withWorkForceMember(Employes.get(34))
									.build())
							.withProject( ProjectFactory.newBuilder()
									.withName("Business Management")
									.withHead(Employes.get(35))
									.withWorkForceMember(Employes.get(36))
									.withWorkForceMember(Employes.get(37))
									.withWorkForceMember(Employes.get(38))
									.withWorkForceMember(Employes.get(39))
									.build())
							.build())
					.build();
		
			
			
			AveragingAgesVisitor V=new AveragingAgesVisitor();
			
			System.out.println(MaBoite);
			MaBoite.departments().forEach(dep->{
				dep.managers().forEach(employe->{
					V.visitEmployee(employe);
				});
			});
			System.out.println("il y a "+V.getCountEmployee()+" managers qui bossent ICI. leur moyenne d'age est: "+V.averageAges());
			
			AveragingAgesVisitor V2=new AveragingAgesVisitor();
			
			MaBoite.departments().forEach(dep->{
				dep.projects().forEach(projet->{
					ContractTypeVisitor V3 = new ContractTypeVisitor();
					projet.workForce().forEach(employe->{
						
						V2.visitEmployee(employe);
						V3.visiteEmployee((EmployeeFactory) employe);
					});
					System.out.println("Pour le projet: "+projet.name());V3.chercheFullTime();
				});
			});
			System.out.println("il y a "+V2.getCountEmployee()+" employés qui bossent sur des proejt ICI. leur moyenne d'age est: "+V2.averageAges());
		} catch (IOException e) {
			
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		 
		
		
	}
}

