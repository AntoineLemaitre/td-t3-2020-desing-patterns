package org.paumard.company;

import java.util.ArrayList;
import java.util.List;

public class DepartmentFactory implements Department {

	private Employee head;
	private String nomDepartement;
	private List<Employee> managers = new ArrayList<>();
	private List<Project> projects = new ArrayList<>();
	
	public static Builder newBuilder() {
		return new Builder();
	}
	
	public static class Builder {

		private Employee head;
		private String nomDepartement;
		private List<Employee> managers = new ArrayList<>();
		private List<Project> projects = new ArrayList<>();;

		public Builder withHead(Employee head) {
			this.head = head;
			return this;
		}
		public Builder withNomDepertement(String head) {
			this.nomDepartement = head;
			return this;
		}

		public Builder withManagementMember(Employee manager) {
			this.managers.add(manager);
			return this;
		}

		public Builder withProject(Project project) {
			this.projects.add(project);
			return this;
		}

		public Department build() {
			DepartmentFactory department = new DepartmentFactory();
			department.head = this.head;
			department.nomDepartement=this.nomDepartement;
			department.managers.addAll(this.managers);
			department.projects.addAll(this.projects);
			return department;
		}
		
		
	}
	
	
	
	
	@Override
	public Employee head() {
		// TODO Auto-generated method stub
		return this.head;
	}

	@Override
	public List<Employee> managers() {
		// TODO Auto-generated method stub
		return List.copyOf(this.managers);
	}

	@Override
	public List<Project> projects() {
		// TODO Auto-generated method stub
		return List.copyOf(this.projects);
	}

	@Override
	public String toString() {
		return "\n\n\n\n\nLe Nom du Département est: "+nomDepartement+"\nLe dirigeant est: " + head + "\nmanagers=" + managers + "\nprojects=" + projects ;
	}

}
